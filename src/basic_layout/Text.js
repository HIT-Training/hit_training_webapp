import React from 'react';

import styled from "styled-components";

export const ComponentTitle = styled.h1`
  margin: 0;
  font-family: 'Montserrat', sans-serif;
  font-weight: lighter;
  font-size: 2em;
  display: inline;
`;

export const ComponentTitleCount = styled(ComponentTitle)`
  padding-right: 0.2em;
  color: blue;
`;

export const ComponentTitleInput = styled.input`
  margin: 0 0 0 0.2em;
  font-family: 'Montserrat', sans-serif;
  font-weight: lighter;
  font-size: 2em;
  border: none;
  display: inline;
`;

export const ComponentSubTitle = styled.h2`
  margin: 0;
  font-family: 'Montserrat', sans-serif;
  font-weight: lighter;
  font-size: 1.8em;
  display: inline;
`;

export const ItemTitle = styled.h3`
  margin: 0;
  font-family: 'Montserrat', sans-serif;
  font-weight: lighter;
  font-size: 1.6em;
  display: inline;
`;

export const ItemSubTitle = styled.h4`
  margin: 0;
  font-family: 'Montserrat', sans-serif;
  font-weight: lighter;
  font-size: 1.4em;
  display: inline;
`;

export const Label = styled.p`
  margin: 0;
  font-family: 'Montserrat', sans-serif;
  font-weight: lighter;
  font-size: 1em;
  display: inline;
`;
