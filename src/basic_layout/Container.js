import React from 'react';

import styled from "styled-components";

export const BasicRowContainer = styled.div`
  display: flex;
  flex-direction: row;
  padding: 1em 1em 0;
`;

export const IconContainer = styled.div`
  flex-grow: 1;
  text-align: right;
`;
