import React from 'react';

import styled from "styled-components";

import { Row, Header } from "../basic_control/Table"
import { IconTable } from "../basic_control/Icon";


const InfoIcon = styled(IconTable)`
  color: lightgray;
`;

export const WorkflowTemplateExerciseListItemSetTableColumnHeader = () => (
  <Row>
    <Header>
      Set Number<InfoIcon className="fas fa-info-circle" />
    </Header>
    <Header>
      Weight (Fraction)<InfoIcon className="fas fa-info-circle" />
    </Header>
    <Header>
      Repetition Goal<InfoIcon className="fas fa-info-circle" />
    </Header>
    <Header>
      Always Include<InfoIcon className="fas fa-info-circle" />
    </Header>
    <Header>
      Inclusion Threshold<InfoIcon className="fas fa-info-circle" />
    </Header>
    <Header />
  </Row>
);
