import React from 'react';

import { Row, Field } from "../basic_control/Table"
import { ButtonItemSubTitle } from "../basic_control/Button";


export const WorkflowTemplateExerciseListItemSetTableRow = (props) => {
  const number = props.number;
  const weightFraction = props.weightFraction;
  const repetitions = props.repetitions;
  const alwaysInclude = props.alwaysInclude;
  const inclusionThreshold = props.inclusionThreshold;
  const alwaysIncludeCheckbox =
    alwaysInclude ? <input type="checkbox" checked /> : <input type="checkbox" />;
  return (
    <Row>
      <Field>{ number }</Field>
      <Field>{ weightFraction }</Field>
      <Field>{ repetitions }</Field>
      <Field>{ alwaysIncludeCheckbox }</Field>
      <Field>{ inclusionThreshold }</Field>
      <Field>
        <ButtonItemSubTitle iconClass="fas fa-minus-circle" />
      </Field>
    </Row>
  );
};
