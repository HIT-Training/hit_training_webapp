import React from 'react';

import styled from "styled-components";

import { WorkflowTemplateTitle } from "./WorkflowTemplateTitle";
import { WorkflowTemplateCreationInfo } from "./WorkflowTemplateCreationInfo";
import { WorkoutTemplateExerciseList } from "./WorkoutTemplateExerciseList";

const WorkoutTemplateDiv = styled.div`
  border: solid thin;
  padding-left: 1em;
  padding-right: 1em;
  padding-bottom: 1em;
`;

export const WorkoutTemplate = (props) => {
  const workoutTemplate = props.workoutTemplate;
  const loading = props.loading;
  return (
    <WorkoutTemplateDiv>
      <WorkflowTemplateTitle
        name={ workoutTemplate.name }
        loading={ loading } />
      <WorkflowTemplateCreationInfo
        userName={ workoutTemplate.createdBy.name }
        creationDate={ workoutTemplate.createdOn } />
      <WorkoutTemplateExerciseList exercises={ workoutTemplate.exercises } />
    </WorkoutTemplateDiv>
  )
};
