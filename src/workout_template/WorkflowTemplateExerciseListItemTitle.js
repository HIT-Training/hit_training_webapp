import React from 'react';

import styled from "styled-components";

import { ItemTitle } from "../basic_layout/Text.js";
import { BasicRowContainer, IconContainer } from "../basic_layout/Container";
import { ButtonItemTitle } from "../basic_control/Button";


export const WorkflowTemplateExerciseListItemTitle = (props) => {
  const title = props.title;
  return (
    <BasicRowContainer>
      <ItemTitle>{ title }</ItemTitle>
      <IconContainer>
        <ButtonItemTitle iconClass="fas fa-times-circle" text="" />
      </IconContainer>
    </BasicRowContainer>
  );
};
