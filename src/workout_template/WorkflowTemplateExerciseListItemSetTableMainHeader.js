import React from 'react';

import styled from "styled-components";

import { BasicRowContainer } from "../basic_layout/Container";
import { Row, Header } from "../basic_control/Table"
import { ItemSubTitle } from "../basic_layout/Text";
import { IconContainer } from "../basic_layout/Container";
import { ButtonItemSubTitle } from "../basic_control/Button";


const MainHeaderRowContainer = styled(BasicRowContainer)`
  padding: 1em;
`;

export const WorkflowTemplateExerciseListItemSetTableMainHeader = () => (
  <Row>
    <Header colSpan="6">
      <MainHeaderRowContainer>
        <ItemSubTitle>Sets</ItemSubTitle>
        <IconContainer>
          <ButtonItemSubTitle iconClass="fas fa-plus-circle" />
        </IconContainer>
      </MainHeaderRowContainer>
    </Header>
  </Row>
);
