import React from 'react';

import styled from "styled-components";

import { WorkflowTemplateExerciseListItemSetTable } from "./WorkflowTemplateExerciseListItemSetTable";
import { WorkflowTemplateExerciseProperties } from "./WorkflowTemplateExerciseProperties";
import { WorkflowTemplateExerciseListItemTitle } from "./WorkflowTemplateExerciseListItemTitle";


const ExerciseListItemDiv = styled.div`
  border: thin solid;
  margin-top: 1em;
`;

export const WorkoutTemplateExerciseListItem = (props) => {
  const name = props.exercise.name;
  const equipment = props.exercise.equipment;
  const increment =
    equipment.weightStack ?
      equipment.weightStack.increment + " " + equipment.weightStack.unit : "";
  const sets = props.exercise.sets;
  return (
    <ExerciseListItemDiv>
      <WorkflowTemplateExerciseListItemTitle title={ name } />
      <WorkflowTemplateExerciseProperties
        equipment={ equipment.name }
        increment={ increment } />
      <WorkflowTemplateExerciseListItemSetTable sets={ sets } />
    </ExerciseListItemDiv>
  );
};
