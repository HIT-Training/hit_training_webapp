import React from 'react';

import { Input } from "../basic_control/Input";
import { BasicRowContainer } from "../basic_layout/Container";

export const WorkflowTemplateCreationInfo = (props) => {
  const userName = props.userName;
  const creationDate = props.creationDate;
  return (
    <BasicRowContainer>
      <Input label={ "Created by" } value={ userName } />
      <Input label={ "Created on" } value={ creationDate } />
    </BasicRowContainer>
  );
};
