import React from 'react';

import styled from "styled-components";

import { BasicRowContainer } from "../basic_layout/Container";
import { Input } from "../basic_control/Input";


export const WorkflowTemplateExerciseProperties = (props) => {
  const equipment = props.equipment;
  const increment = props.increment;
  return (
    <BasicRowContainer>
      <Input label={ "Equipment" } value={ equipment } />
      <Input label={ "Increment" } value={ increment } />
    </BasicRowContainer>
  );
};
