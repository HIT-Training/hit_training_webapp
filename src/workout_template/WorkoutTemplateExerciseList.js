import React from 'react';

import styled from "styled-components";

import { ComponentSubTitle, ComponentTitleCount } from "../basic_layout/Text";
import { BasicRowContainer, IconContainer } from "../basic_layout/Container";
import { WorkoutTemplateExerciseListItem } from "./WorkoutTemplateExerciseListItem";


const ExerciseListDiv = styled.div`
  border: solid thin;
  margin-top: 1em;
  padding-left: 1em;
  padding-right: 1em;
  padding-bottom: 1em;
  flex-grow: 1;
`;

export class WorkoutTemplateExerciseList extends React.Component {
  render () {
    const exercises = this.props.exercises;
    const count = exercises.length;
    const exerciseList = this.createExerciseList(exercises);
    return (
      <ExerciseListDiv>
        <BasicRowContainer>
          <ComponentSubTitle>Exercises</ComponentSubTitle>
          <IconContainer>
            <ComponentTitleCount>{ count }</ComponentTitleCount>
          </IconContainer>
        </BasicRowContainer>
        { exerciseList }
      </ExerciseListDiv>
    );
  };

  createExerciseList(exercises) {
    return exercises.map((exercise, index) => (
      <WorkoutTemplateExerciseListItem
        key={ index }
        exercise={ exercise } />
    ));
  }
}
