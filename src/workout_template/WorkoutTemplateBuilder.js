import React from 'react';

import styled from "styled-components";

import {ExerciseList} from "../exercise/ExerciseList";
import {WorkoutTemplate} from "./WorkoutTemplate";


const WorkoutTemplateBuilderDiv = styled.div`
  padding: 1em;
  display: flex;
  flex-direction: row;
`;

const ExerciseListContainer = styled.div`
  padding-right: 1em;
  flex-grow: 1;
`;

const WorkoutTemplateContainer = styled.div`
  flex-grow: 3;
`;

export class WorkoutTemplateBuilder extends React.Component {
  constructor() {
    super();
    this.state = {
      exercises: [],
      workoutTemplate: {
        name: "Full Body Workout A",
        createdBy: {
          name: "Frank Krick"
        },
        createdOn: "9/8/2018",
        exercises: []
      },
      loadingExercises: false,
      loadingWorkoutTemplates: false,
      updatingExercisesSet: new Set()
    };

    this.onAssignExercise = this.onAssignExercise.bind(this);
  }

  componentDidMount() {
    this.loadExercises();
  }

  loadExercises() {
    const url = '/hit/exercise';

    this.setState({ loadingExercises: true });
    this.loadExercisesUrl(url)
  }

  loadExercisesUrl(url) {
    fetch(url)
      .then(response => response.json())
      .then(data => {
        const exercises = this.state.exercises.concat(data.exercises);
        if (data.next) {
          this.setState({
            exercises: exercises
          });
          this.loadExercisesUrl(data.next)
        } else {
          this.setState({
            exercises: exercises,
            loadingExercises: false
          });
        }
      });
  }

  onAssignExercise(exercise, event) {
    this.assignExercise(exercise)
  }

  assignExercise(exercise) {
    this.addExerciseToWorkoutTemplate(exercise);
    this.deleteExerciseFromExerciseList(exercise);
    this.updateExerciseData(exercise)
  }

  updateExerciseData(exercise) {
    let updatingExercisesSet = this.state.updatingExercisesSet.add(exercise._id);
    this.setState({ updatingExercisesSet: updatingExercisesSet });
    this.updateWorkoutTemplateLoadingIndicator();
    const url = '/hit/equipment/' + exercise.equipment.id;
    fetch(url)
      .then(response => response.json())
      .then(data => {
        debugger;
        let workoutTemplate = this.state.workoutTemplate;
        const index = workoutTemplate.exercises.findIndex(e => e.id === exercise._id);
        workoutTemplate.exercises[index].equipment = data;
        this.setState({ workoutTemplate: workoutTemplate });
        let updatingExercisesSet = this.state.updatingExercisesSet;
        updatingExercisesSet.delete(exercise._id);
        this.setState({ updatingExercisesSet: updatingExercisesSet });
        this.updateWorkoutTemplateLoadingIndicator();
      });
  }

  updateWorkoutTemplateLoadingIndicator() {
    if (this.state.updatingExercisesSet.size > 0) {
      this.setState({ loadingWorkoutTemplates: true });
    } else {
      this.setState({ loadingWorkoutTemplates: false });
    }
  }

  deleteExerciseFromExerciseList(exercise) {
    const exercises = this.state.exercises.filter(e => e._id !== exercise._id);
    this.setState({ exercises: exercises });
  }

  addExerciseToWorkoutTemplate(exercise) {
    const workoutTemplateExercise = {
      id: exercise._id,
      name: exercise.name,
      equipment: {
        id: exercise.equipment.id,
        name: exercise.equipment.name,
      },
      sets: [
        {
          weightFraction: 1,
          repetitions: 10,
          alwaysInclude: true,
          inclusionThreshold: ""
        }
      ]
    };
    let workoutTemplate = this.state.workoutTemplate;
    workoutTemplate.exercises = workoutTemplate.exercises.concat(workoutTemplateExercise);
    this.setState({ workoutTemplate: workoutTemplate });
  }

  render() {
    const workoutTemplate = this.state.workoutTemplate;
    const exercises = this.state.exercises;
    const loading = this.state.loadingExercises;
    const loadingWorkoutTemplates = this.state.loadingWorkoutTemplates;
    return (
      <WorkoutTemplateBuilderDiv>
        <ExerciseListContainer>
          <ExerciseList
            exercises={ exercises }
            loading={ loading }
            onAssignExercise={ this.onAssignExercise }/>
        </ExerciseListContainer>
        <WorkoutTemplateContainer>
          <WorkoutTemplate
            workoutTemplate={ workoutTemplate }
            loading={ loadingWorkoutTemplates } />
        </WorkoutTemplateContainer>
      </WorkoutTemplateBuilderDiv>
    );
  }
}
