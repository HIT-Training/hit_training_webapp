import React from 'react';

import { WorkflowTemplateExerciseListItemSetTableMainHeader } from "./WorkflowTemplateExerciseListItemSetTableMainHeader";
import { WorkflowTemplateExerciseListItemSetTableColumnHeader } from "./WorkflowTemplateExerciseListItemSetTableColumnHeader";
import { WorkflowTemplateExerciseListItemSetTableRow } from "./WorkflowTemplateExerciseListItemSetTableRow";
import { BasicRowContainer } from "../basic_layout/Container";
import { Table } from "../basic_control/Table"


export class WorkflowTemplateExerciseListItemSetTable extends React.Component {
  render() {
    const sets = this.props.sets;
    const tableRows = this.createTableRows(sets);
    return (
      <BasicRowContainer>
        <Table>
          <tbody>
            <WorkflowTemplateExerciseListItemSetTableMainHeader />
            <WorkflowTemplateExerciseListItemSetTableColumnHeader />
            { tableRows }
          </tbody>
        </Table>
      </BasicRowContainer>
    )
  }

  createTableRows(sets) {
    return sets.map((set, index) => (
      <WorkflowTemplateExerciseListItemSetTableRow
        key={ index }
        number={ index + 1 }
        weightFraction={ set.weightFraction }
        repetitions={ set.repetitions }
        alwaysInclude={ set.alwaysInclude }
        inclusionThreshold={ set.inclusionThreshold } />
    ));
  }
}
