import React from 'react';

import styled from "styled-components";
import { keyframes } from "styled-components";

import { ComponentTitle, ComponentTitleInput } from "../basic_layout/Text";
import { BasicRowContainer, IconContainer } from "../basic_layout/Container";
import { ButtonComponentTitle } from "../basic_control/Button";
import { IconComponentTitle } from "../basic_control/Icon";


const WorkoutTemplateNameInput = styled(ComponentTitleInput)`
  flex-grow: 1;
`;

const rotation = keyframes`
  from {
    transform: rotate(0deg);
  }
  
  to {
    transform: rotate(360deg);
  }
`;

const LoadingIcon = styled(IconComponentTitle)`
  color: lightgray;
  animation: ${rotation} 2s linear infinite;
  margin-right: 0.3em;
`;

export class WorkflowTemplateTitle extends React.Component {
  render() {
    const name = this.props.name;
    const loading = this.props.loading;
    const iconContainer = this.createLoadingindicatorOrButtons(loading);
    return (
      <BasicRowContainer>
        <ComponentTitle>Workout Template:</ComponentTitle>
        <WorkoutTemplateNameInput value={name}/>
        { iconContainer }
      </BasicRowContainer>
    );
  };

  createLoadingindicatorOrButtons(loading) {
    if (loading) {
      return (
        <IconContainer>
          <LoadingIcon className="fas fa-sync" />
          <ButtonComponentTitle iconClass="fas fa-times-circle"/>
        </IconContainer>
      );
    } else {
      return (
        <IconContainer>
          <ButtonComponentTitle iconClass="fas fa-check-circle"/>
          <ButtonComponentTitle iconClass="fas fa-times-circle"/>
        </IconContainer>
      );
    }
  }
}
