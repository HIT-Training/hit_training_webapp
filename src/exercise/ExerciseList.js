import React from 'react';

import styled from 'styled-components';
import { keyframes } from "styled-components";
import { curry } from 'ramda';

import { ComponentTitle, ComponentTitleCount } from "../basic_layout/Text";
import { BasicRowContainer, IconContainer } from "../basic_layout/Container";
import { ExerciseListItem } from "./ExerciseListItem";
import { IconComponentTitle } from "../basic_control/Icon";


const ExerciseListDiv = styled.div`
  border: solid thin;
  padding-left: 1em;
  padding-right: 1em;
  padding-bottom: 1em;
`;

const rotation = keyframes`
  from {
    transform: rotate(0deg);
  }
  
  to {
    transform: rotate(360deg);
  }
`;

const LoadingIcon = styled(IconComponentTitle)`
  color: lightgray;
  animation: ${rotation} 2s linear infinite;
`;

export const ExerciseList = (props) => {
  const loading = props.loading;
  const exercises = props.exercises;
  const exerciseList = exercises.map((exercise, index) => {
    const onAssignExercise = curry(props.onAssignExercise)(exercise);
    return <ExerciseListItem
              name={ exercise.name }
              equipmentName={ exercise.equipment.name }
              key={ index }
              onAssignExercise={ onAssignExercise } />
  });
  const loadingIndicator = loading ? <LoadingIcon className="fas fa-sync" /> : null;
  const numberOfExercises = exercises.length;
  return (
    <ExerciseListDiv>
      <BasicRowContainer>
        <ComponentTitle>Exercises</ComponentTitle>
        <IconContainer>
          <ComponentTitleCount>{ numberOfExercises }</ComponentTitleCount>
          { loadingIndicator }
        </IconContainer>
      </BasicRowContainer>
      { exerciseList }
    </ExerciseListDiv>
  );
};
