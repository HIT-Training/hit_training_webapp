import React from 'react';

import styled from "styled-components";

import { IconContainer, BasicRowContainer } from "../basic_layout/Container";
import { ButtonItemTitle } from "../basic_control/Button";
import { ItemTitle } from "../basic_layout/Text";
import { Input } from "../basic_control/Input";


const ExerciseListItemDiv = styled.div`
  border: thin solid;
  margin-top: 1em;
`;

export const ExerciseListItem = (props) => {
  const name = props.name;
  const equipmentName = props.equipmentName;
  const onAssignExercise = props.onAssignExercise;
  return (
    <ExerciseListItemDiv draggable>
      <BasicRowContainer>
        <ItemTitle>{ name }</ItemTitle>
        <IconContainer>
          <ButtonItemTitle
            iconClass="fas fa-arrow-circle-right"
            onClick={ onAssignExercise } />
        </IconContainer>
      </BasicRowContainer>
      <BasicRowContainer>
        <Input label="Equipment" value={ equipmentName } />
      </BasicRowContainer>
      <BasicRowContainer/>
    </ExerciseListItemDiv>
  );
};
