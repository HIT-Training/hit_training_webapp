import React from 'react';

import styled from "styled-components";

import { Label } from "../basic_layout/Text";

const InputRowDiv = styled.div`
  display: flex;
  flex-direction: row;
  margin: 0;
  padding: 0;
  flex-grow: 1;
`;

const InputField = styled.input`
  margin: 0;
  padding: 0 0 0 0.3em;
  border: none;
  font-family: 'Montserrat', sans-serif;
  font-weight: bold;
  font-size: 1em;
`;

export const Input = (props) => {
  return (
    <InputRowDiv>
      <Label>{ props.label }</Label>
      <InputField value={ props.value } />
    </InputRowDiv>
  );
};
