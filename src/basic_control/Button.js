import React from 'react';

import { curry } from 'ramda';

import { IconItemTitle, IconItemSubTitle, IconComponentTitle } from "./Icon";


const ButtonBase = (Icon, props) => {
  const iconClass = props.iconClass;
  const text = props.text;
  const onClick = props.onClick ? props.onClick : () => { console.log("No handler") };
  return (
    <button onClick={ onClick }>
      <Icon className={ iconClass } />
      { text }
    </button>
  );
};

const ButtonBaseCurry = curry(ButtonBase);

export const ButtonItemTitle = ButtonBaseCurry(IconItemTitle);
export const ButtonItemSubTitle = ButtonBaseCurry(IconItemSubTitle);
export const ButtonComponentTitle = ButtonBaseCurry(IconComponentTitle);
