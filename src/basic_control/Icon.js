import React from 'react';

import styled from "styled-components";

export const IconComponentTitle = styled.i`
  font-size: 2em;
`;

export const IconItemTitle = styled.i`
  font-size: 1.6em;
`;

export const IconItemSubTitle = styled.i`
  font-size: 1.4em;
`;

export const IconTable = styled.i`
  font-size: 1em;
  margin-left: 0.3em;
`;
