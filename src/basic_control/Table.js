import React from 'react';

import styled from "styled-components";

export const Table = styled.table`
  flex-grow: 1;
  border: solid thin;
  margin-bottom: 1em;
  border-collapse: collapse;
  font-family: 'Montserrat', sans-serif;
`;

export const Row = styled.tr`
  height: 3em;
`;

export const Header = styled.th`
  border: solid thin;
  font-size: 1em;
  font-weight: bold;
`;

export const Field = styled.td`
  border: solid thin;
  border-bottom: 0;
  border-top: 0;
`;
