import React, { Component } from 'react';

import { WorkoutTemplateBuilder } from "./workout_template/WorkoutTemplateBuilder";

import logo from './logo.svg';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">HIT Training</h1>
        </header>
        <WorkoutTemplateBuilder />
      </div>
    );
  }
}

export default App;
